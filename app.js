const express = require('express')
const app = express()
const dialogflow = require("./dialogflow.js")

const twilio = require("./twilio")
app.use(express.json());
app.use(express.urlencoded({extended:true}));
 
app.post('/webhook', async function (req, res) {
  console.log("req",req.body);
  let phone= req.body.WaId;
 console.log(phone);
  let receivedMessage = req.body.Body;
  let payload =await dialogflow.sendToDialogFlow(receivedMessage,"aaa")
  let responses = payload.fulfillmentMessages;
  console.log("FULL",responses);

  for(const response of responses){
    console.log("veo resp",response.text.text[0]);
    await  twilio.sendTextMessage(phone, response.text.text[0]);
  }

 
  res.status(200).json({ok:true,msg:"Mensaje enviado correctamente"});
})
 
app.listen(3000,()=>{
    console.log("Servidor de ANA Corriendo");
})